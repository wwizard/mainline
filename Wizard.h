#ifndef EXT_WIZARD_H_
#define EXT_WIZARD_H_

#include <Wt/Ext/Dialog>
#include <boost/any.hpp>
#include <vector>
#include <Wt/WText>
#include <map>
// #include "WizardPage.h"

namespace Wt {

  class WContainerWidget;
  class WBorderLayout;

  namespace Ext {

  class Button;
  class ToolBar;
  class WizardPage;
  class WizardField;

/*! \class Wizard Ext/Wizard Ext/Wizard
 *  \brief A framework to create multipage wizards
 *
 * The Wizard class provides a framework for wizards.
 *
 * A wizard (also called an assistant on Mac OS X) is a special type of input
 * dialog that consists of a sequence of pages. A wizard's purpose is to
 * guide the user through a process step by step. Wizards are useful for
 * complex or infrequent tasks that users may find difficult to learn.
 *
 * Wizard inherits Ext/Panel and represents a wizard. Each page is a
 * WizardPage (an Ext/Container subclass).
 */
  class WT_EXT_API Wizard : public Wt::Ext::Dialog
  {

    public:
    Wizard( const Wt::WString& wizardTitle = WString() );
    ~Wizard();

    int addPage( Wt::Ext::WizardPage *page );
    Wt::Ext::WizardPage* currentPage() const;
    void setWindowTitle(const Wt::WString &windowTitle);
    bool hasVisitedPage( unsigned int page ) const;
    std::vector<unsigned int> visitedPages() const;
    void setWizard( Wt::Ext::WizardPage* page ); // FIXME This method should be private
    bool addField(Wt::Ext::WizardField field); // FIXME This method should be private
//     bool addPendingField(Wt::Ext::WizardField field); // FIXME This method should be private
    void addPendingFieldsFromPage( Wt::Ext::WizardPage *page); // FIXME This method should be private

    public slots:
    boost::any field(std::string name);
    virtual void next();
    void back();
    void restart();
    virtual void show();

    protected:
    virtual void initializePage( unsigned int index );
    virtual void cleanupPage( unsigned int index );

    private:
    Wt::Ext::ToolBar *toolBar_;
    unsigned int currentPageIndex_;
    Wt::Ext::WizardPage *currentPage_;
    WContainerWidget* pageContainer_;
    std::vector<WizardPage*> pageList_;
    std::vector<unsigned int> visitedPages_;
    std::map<std::string, Wt::Ext::WizardField> fields_;
    void buildToolBar();
    Wt::Ext::Button *back_;
    Wt::Ext::Button *next_;
    Wt::Ext::Button *finish_;
    Wt::Ext::Button *cancel_;
    void setPage(Wt::Ext::WizardPage *page);
    Wt::WText* pageTitle_;
  };

  }
}

#endif
