#ifndef EXT_WIZARDPAGE_H_
#define EXT_WIZARDPAGE_H_

#include <Wt/WContainerWidget>
#include <Wt/Ext/Panel>
#include <Wt/WString>
#include "Wizard.h"

#define FIELD(name, instance, method) std::string( #name), boost::bind( &method, boost::cref(instance) )

namespace Wt {

  namespace Ext {

  class WT_EXT_API WizardPage : public Wt::WContainerWidget
  {

    friend void Wt::Ext::Wizard::setWizard( Wt::Ext::WizardPage* page );
    friend void Wt::Ext::Wizard::addPendingFieldsFromPage( Wt::Ext::WizardPage* page );

    public:
    WizardPage( Wt::WContainerWidget* parent = 0 );
    WizardPage( const Wt::WString& title, Wt::WContainerWidget* parent = 0 );
    ~WizardPage();
    // Use it as:  page->registerField( FIELD( testX, x, X::text ) ); (please note the field name must not be between quotes)
    void registerField( const std::string fieldName, boost::function<boost::any()> fieldMethod );
    virtual bool validatePage();
    void setTitle(const Wt::WString& t);
    Wt::WString title();
    bool isFinalPage();
    void setFinalPage( bool f );
    virtual void initializePage(); // This method is called by Ext::Wizard initializePage() to prepare the page before it is shown as a result of the user clicking 'Next'. Users inheriting from Ext::WizardPage should reimplement this method to set initial values for the page.

    virtual void cleanupPage(); // This method is called by Ext::Wizard cleanupPage() when the user leaves the page by clicking 'Back'. The default implementation resets the page fields to their original values (the values they had before initializePage() was called).

    protected:
//     void registerField( const std::string& name, WObject * object, const std::string& method, const std::string& changedSignal = 0);
    void setField( const std::string& name, const boost::any& value );
    boost::any field( const std::string& name) const;
    bool addPendingField(Wt::Ext::WizardField field);

    private:
    Wt::WString title_;
    bool finalPage_;
    Wt::Ext::Wizard* wizard_;
    std::map<const std::string, Wt::Ext::WizardField> pendingFields_;

  };

  }
}

#endif
