#include "Wizard_p.h"

Wt::Ext::WizardField::WizardField() { ; }

Wt::Ext::WizardField::WizardField(
            Wt::Ext::WizardPage* page,
            const std::string name,
            boost::function<boost::any()> fieldMethod /*,
            const std::string& changedSignal */
        ) : page_(page),
            fieldName_(name),
            fieldMethod_(fieldMethod) /*,
            changedSignal_(changedSignal) */,
            mandatory_(false)
    {
        if( boost::algorithm::ends_with( fieldName_, "*") ) {
            mandatory_ = true;
            boost::algorithm::trim_right_if( fieldName_, boost::algorithm::is_any_of("*") );
        }
    }

const boost::any Wt::Ext::WizardField::field() const {
    return boost::any( fieldMethod_() );
}
