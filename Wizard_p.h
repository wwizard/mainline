#ifndef EXT_WIZARD_P_H_
#define EXT_WIZARD_P_H_

#include <boost/algorithm/string.hpp>
#include <boost/algorithm/string/compare.hpp>
#include <string>
#include <Wt/WObject>

// DO NOT INSTALL THIS HEADER FILE IN YOUR SYSTEM
// DO NOT USE THIS HEADER FILE IN YOUR APPLICATIONS
//
// WizardField is an internal class needed to implement registerField, field,
// setField in a way that they support mandatory fields, initial values, etc

namespace Wt {
    namespace Ext {

    class WizardPage;

class WizardField {

//     friend class Wt::Ext::Wizard;

public: // Using std::string instead of WString because boost::algorithm string library won't work with WString due to missing range_iterator and const_iterator
    WizardField();
    WizardField( Wt::Ext::WizardPage* page, const std::string fieldName, boost::function<boost::any()> fieldMethod /*, const std::string& changedSignal */);
    const boost::any field() const;

    Wt::Ext::WizardPage* page_;
    std::string fieldName_;
    boost::function<boost::any()> fieldMethod_;
//     std::string changedSignal_;
    bool mandatory_;
};

    }
}

#endif
