#include <Wt/Ext/ToolBar>
#include <Wt/WBorderLayout>
#include <Wt/WContainerWidget>
#include <Wt/WImage>
#include <iostream>
#include <Wt/WBreak>
#include <algorithm>
#include "Wizard.h"
#include "WizardPage.h"
#include "Wizard_p.h"

// namespace Wt {
//   namespace Ext {


Wt::Ext::Wizard::Wizard( const Wt::WString& windowTitle ) :
    Dialog( windowTitle ),
    toolBar_(0),
    currentPageIndex_(-1),
    currentPage_(0),
    pageContainer_(0)
{
    resize(Wt::WLength(600), Wt::WLength(400)); // TODO Find out why setMinimumSize does not work
    buildToolBar();
    pageContainer_ = new WContainerWidget();
    contents()->addWidget( pageContainer_ );
}

Wt::Ext::Wizard::~Wizard()
{ }

// Returns an integer representing what *page number* (not what position in the vector containing the list of pages) was added
int Wt::Ext::Wizard::addPage( Wt::Ext::WizardPage *page) {

    if( 0 == page) { // Do not add null pointers
        return -1;
    }

    setWizard( page );
    addPendingFieldsFromPage( page );
    pageList_.push_back(page);
    return pageList_.size();

}

Wt::Ext::WizardPage* Wt::Ext::Wizard::currentPage() const {
    return pageList_.at(currentPageIndex_); // Position in the pageList_ vector is currentPageIndex_, as currentPageIndex_ = -1 means the wizard has not been started
}

void Wt::Ext::Wizard::setWindowTitle(const Wt::WString &windowTitle) {

    // FIXME Really ugly. Ask Emweb for a Dialog::setTitle(const WText& code) or even Dialog::setTitle( WWidget* w )
    //     Dialog::windowTitle()->setStyleClass("wizardTitle"); // Will the possible when Emweb provides the above setTitle( const WText& code )
    Dialog::setWindowTitle("<br/>&nbsp;&nbsp;<img src=\"/wizard.png\"/>&nbsp;&nbsp;"+windowTitle+"<br/>");

}

bool Wt::Ext::Wizard::hasVisitedPage( unsigned int pageIndex ) const {
    std::vector<unsigned int>::const_iterator iter;
    for (iter = visitedPages_.begin(); iter != visitedPages_.end(); iter++ ) {
      if( pageIndex == *iter ) {
        return true; // We don't care how many times that page was visited, only that it was visited at least once; therefore, exit as soon as we are certain it has been visited
      }
    }
    return false;
}

std::vector<unsigned int> Wt::Ext::Wizard::visitedPages() const {
    return visitedPages_;
}

boost::any Wt::Ext::Wizard::field(std::string name) {

#ifdef _DEBUG
    std::map<std::string, Wt::Ext::WizardField>::const_iterator iter;
    std::cerr << "The following fields are registered:\n";
    for( iter=fields_.begin(); iter != fields_.end(); iter++ ) {
            std::cerr << "Field: " << iter->first << "\n";
        }
#endif

    return fields_[name].field();
}

void Wt::Ext::Wizard::next() {

    currentPageIndex_++;
    initializePage( currentPageIndex_ );
    setPage( pageList_.at(currentPageIndex_) );

}

void Wt::Ext::Wizard::back() {
    currentPageIndex_--;
    cleanupPage( currentPageIndex_ );
    setPage( pageList_.at( currentPageIndex_ ) );
}

void Wt::Ext::Wizard::restart() {

    // Initialize all pages

    std::vector<Wt::Ext::WizardPage*>::iterator iter;
    for( iter = pageList_.begin(); iter != pageList_.end(); iter++ ) {
        (*iter)->initializePage();
    }

    // Reset index
    currentPageIndex_ = -1;

    show();
}

void Wt::Ext::Wizard::show() {

    if( !pageList_.empty() ) { // If no page was added, show nothing and return a null pointer

    /*wizard.setMinimumSize( Wt::WLength(600), Wt::WLength(400) );*/ // FIXME Remove in the final implementation. This is only temporary, to have a minimum size

    pageList_.at( pageList_.size() - 1 )->setFinalPage(true); // Last page in the vector is always a final page

    next();

    Wt::Ext::Dialog::exec();
    }
}

void Wt::Ext::Wizard::initializePage( unsigned int index ) {
    pageList_.at(index)->initializePage();
}

void Wt::Ext::Wizard::cleanupPage( unsigned int index ) {
    pageList_.at(index)->cleanupPage();
}

void Wt::Ext::Wizard::addPendingFieldsFromPage( Wt::Ext::WizardPage *page) {
    // Check if there are pending fields and register them
    if( !page->pendingFields_.empty() ) {
        std::cerr << "Registering pending fields\n";
        std::map<std::string, Wt::Ext::WizardField>::const_iterator iter;
        for( iter=page->pendingFields_.begin(); iter != page->pendingFields_.end(); iter++ ) {
            std::cerr << "Registering pending field: " << iter->first << "\n";
            addField(iter->second);
        }
        page->pendingFields_.clear();
    }

}

void Wt::Ext::Wizard::buildToolBar() {

    back_ = new Wt::Ext::Button("Back");
    back_->activated().connect(SLOT(this, Wt::Ext::Wizard::back));
    addButton(back_);

    next_ = new Wt::Ext::Button("Next");
    next_->activated().connect(SLOT(this, Wt::Ext::Wizard::next));
    next_->setDefault(true);
    addButton(next_);

    finish_ = new Wt::Ext::Button("Finish");
    finish_->activated().connect(SLOT(this, Ext::Dialog::accept));
    addButton(finish_);

    cancel_ = new Wt::Ext::Button("Cancel");
    cancel_->activated().connect(SLOT(this, Ext::Dialog::reject));
    addButton(cancel_);

}

void Wt::Ext::Wizard::setPage(Wt::Ext::WizardPage *page) {

    if( 0 != currentPage_ ) {
        pageContainer_->removeWidget( currentPage_ ); // TODO Free the memory! removeWidget is not freeing it
    }

    currentPage_ = page;
    visitedPages_.push_back( currentPageIndex_ );
    pageContainer_->clear();
    pageTitle_ = new Wt::WText( currentPage_->title(), pageContainer_ );
    new Wt::WBreak( pageContainer_ );
    pageTitle_->setStyleClass("wizardPageTitle");
    pageContainer_->addWidget( currentPage_ );

    if( currentPage_->isFinalPage() ) {
        next_->disable();
//         next_->hide(); // This hide() works, why do finish_->hide() and back_->hide() not work!?
        finish_->show();
        finish_->enable();
    } else {
        if( next_->isHidden() ) {
            next_->show();
        }

        if( !next_->isEnabled() ) {
            next_->enable();
        }

        if( !finish_->isHidden() ) {
            finish_->disable();
//             finish_->hide(); // FIXME If this line is uncommented, Wt shows an error about undefined properties and code :-?
        }

    }
//
    if( 0 == currentPageIndex_ ) {
        back_->disable();
//         back_->hide(); // FIXME If this line is uncommented, Wt shows an error about undefined properties and code :-?
    } else if( !back_->isEnabled() ) {
        back_->enable();
        back_->show();
    }

}

void Wt::Ext::Wizard::setWizard( Wt::Ext::WizardPage* page ) {
    page->wizard_ = this;
}

bool Wt::Ext::Wizard::addField(Wt::Ext::WizardField field) {
    std::pair<std::map<std::string, Wt::Ext::WizardField>::iterator,bool> ret;
    ret = fields_.insert( std::pair<std::string, Wt::Ext::WizardField>(field.fieldName_, field) );
    if( false == ret.second ) {
        std::cerr << "Field " << field.fieldName_ << " already exists\n";
        return false;
    }
    std::cerr << "Added field " << field.fieldName_ << "\n";
    return true;
}
