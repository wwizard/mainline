# Find Witty includes and libraries

#
# Witty_INCLUDE_DIR
# Witty_LIBRARIES  - Release libraries
# Witty_FOUND  - True if release libraries found
# Witty_DEBUG_LIBRARIES  - Debug libraries
# Witty_DEBUG_FOUND  - True if debug libraries found


#
# Copyright (c) 2007, 2008 Pau Garcia i Quiles, <pgquiles@elpauer.org>
#
# Redistribution and use is allowed according to the terms of the BSD license.
# For details see the accompanying COPYING-CMAKE-SCRIPTS file.

FIND_PATH( Witty_INCLUDE_DIR NAMES WObject PATHS ENV PATH PATH_SUFFIXES include wt witty )

SET( Witty_FIND_COMPONENTS Release Debug )

IF( Witty_INCLUDE_DIR )
        FIND_LIBRARY( Witty_LIBRARY NAMES wt PATHS PATH_SUFFIXES lib lib-release lib_release )
        FIND_LIBRARY( Witty_EXT_LIBRARY NAMES wtext PATHS PATH_SUFFIXES lib lib-release lib_release )
        FIND_LIBRARY( Witty_HTTP_LIBRARY NAMES wthttp PATHS PATH_SUFFIXES lib lib-release lib_release )
        FIND_LIBRARY( Witty_FCGI_LIBRARY NAMES wtfcgi PATHS PATH_SUFFIXES lib lib-release lib_release )

        FIND_LIBRARY( Witty_DEBUG_LIBRARY NAMES wt PATHS /usr/lib/debug/usr/lib/ NO_DEFAULT_PATH )
        FIND_LIBRARY( Witty_EXT_DEBUG_LIBRARY NAMES wtext PATHS /usr/lib/debug/usr/lib/ NO_DEFAULT_PATH )
        FIND_LIBRARY( Witty_HTTP_DEBUG_LIBRARY NAMES wthttp PATHS /usr/lib/debug/usr/lib/ NO_DEFAULT_PATH )
        FIND_LIBRARY( Witty_FCGI_DEBUG_LIBRARY NAMES wtfcgi PATHS /usr/lib/debug/usr/lib/ NO_DEFAULT_PATH )

        IF( Witty_LIBRARY AND Witty_EXT_LIBRARY AND Witty_HTTP_LIBRARY AND Witty_FCGI_LIBRARY )
                SET( Witty_FOUND TRUE )
		SET( Witty_FIND_REQUIRED_Release TRUE )
                SET( Witty_LIBRARIES ${Witty_LIBRARY} ${Witty_EXT_LIBRARY} ${Witty_HTTP_LIBRARY} ${Witty_FCGI_LIBRARY} )
        ENDIF( Witty_LIBRARY AND Witty_EXT_LIBRARY AND Witty_HTTP_LIBRARY AND Witty_FCGI_LIBRARY )

        IF( Witty_DEBUG_LIBRARY AND Witty_EXT_DEBUG_LIBRARY AND Witty_HTTP_DEBUG_LIBRARY AND Witty_FCGI_DEBUG_LIBRARY )
                SET( Witty_DEBUG_FOUND TRUE )
		SET( Witty_FIND_REQUIRED_Debug TRUE )
                SET( Witty_DEBUG_LIBRARIES ${Witty_DEBUG_LIBRARY} ${Witty_EXT_DEBUG_LIBRARY} ${Witty_HTTP_DEBUG_LIBRARY} ${Witty_FCGI_DEBUG_LIBRARY} )
        ENDIF( Witty_DEBUG_LIBRARY AND Witty_EXT_DEBUG_LIBRARY AND Witty_HTTP_DEBUG_LIBRARY AND Witty_FCGI_DEBUG_LIBRARY )

        IF(Witty_FOUND)
                IF (NOT Witty_FIND_QUIETLY)
                        MESSAGE(STATUS "Found the Wt libraries at ${Witty_LIBRARIES}")
                        MESSAGE(STATUS "Found the Wt headers at ${Witty_INCLUDE_DIR}")
                ENDIF (NOT Witty_FIND_QUIETLY)
        ELSE(Witty_FOUND)
                IF(Witty_FIND_REQUIRED)
                        MESSAGE(FATAL_ERROR "Could NOT find Wt")
                ENDIF(Witty_FIND_REQUIRED)
        ENDIF(Witty_FOUND)

        IF(Witty_DEBUG_FOUND)
                IF (NOT Witty_FIND_QUIETLY)
                        MESSAGE(STATUS "Found the Wt debug libraries at ${Witty_DEBUG_LIBRARIES}")
                        MESSAGE(STATUS "Found the Wt debug headers at ${Witty_INCLUDE_DIR}")
                ENDIF (NOT Witty_FIND_QUIETLY)
        ELSE(Witty_DEBUG_FOUND)
                IF(Witty_FIND_REQUIRED_Debug)
                        MESSAGE(FATAL_ERROR "Could NOT find Wt debug libraries")
                ENDIF(Witty_FIND_REQUIRED_Debug)
        ENDIF(Witty_DEBUG_FOUND)

ENDIF( Witty_INCLUDE_DIR )
