#include "WizardPage.h"
#include "Wizard_p.h"
#include <iostream>

// #include <WContainerWidget>

namespace Wt {
  namespace Ext {

// WWizardPage

Wt::Ext::WizardPage::WizardPage( Wt::WContainerWidget* parent ) :
        Wt::WContainerWidget(parent),
        title_(""),
        finalPage_(false),
        wizard_(0)
    {
    }

Wt::Ext::WizardPage::WizardPage( const Wt::WString& title, Wt::WContainerWidget* parent ) :
        Wt::WContainerWidget(parent),
        title_(title),
        finalPage_(false)
    {
    }

Wt::Ext::WizardPage::~WizardPage()
{ }

void Wt::Ext::WizardPage::registerField( const std::string fieldName, boost::function<boost::any()> fieldMethod ) {

    Wt::Ext::WizardField f( this, fieldName, fieldMethod /*, changedSignal */);
    if( wizard_ ) {
        wizard_->addField(f);
    } else {
        addPendingField(f);
    }
}

bool Wt::Ext::WizardPage::validatePage() {
    return true; // TODO (needs mandatory fields check)
}

void Wt::Ext::WizardPage::setTitle(const Wt::WString& t) {
    title_ = t;
}

Wt::WString Wt::Ext::WizardPage::title() {
    return title_;
}

bool Wt::Ext::WizardPage::isFinalPage() {
    return finalPage_;
}

void Wt::Ext::WizardPage::setFinalPage( bool f ) {
    finalPage_ = f;
}

void Wt::Ext::WizardPage::initializePage() {
    ; // By default, do nothing
}

void Wt::Ext::WizardPage::cleanupPage() {
    // TODO (needs registerField, field and setField)
}

void Wt::Ext::WizardPage::setField( const std::string& name, const boost::any& value ) {
    // TODO
}

boost::any Wt::Ext::WizardPage::field( const std::string& name) const {
    if( wizard_ ) { // Should we check or just segfault?
        return wizard_->field(name);
    } else {
        return 0;
    }
}

bool Wt::Ext::WizardPage::addPendingField(Wt::Ext::WizardField field) {
    std::pair<std::map<const std::string, Wt::Ext::WizardField>::iterator,bool> ret;
    std::cout << "fieldname: " << field.fieldName_ << std::endl;
    ret = pendingFields_.insert( std::pair<const std::string, Wt::Ext::WizardField>(field.fieldName_, field) );
    if( false == ret.second ) {
        std::cerr << "Field " << field.fieldName_ << " already exists in this page\n";
        return false;
    }
    return true;
}

   }
}
