#include "Wizard.h"
#include "WizardPage.h"

#include <Wt/WApplication>
#include <Wt/WEnvironment>
#include <Wt/Ext/Button>
#include <Wt/Ext/LineEdit>
#include <Wt/WLabel>
#include <Wt/WText>
#include <Wt/WBorderLayout>
#include <Wt/WContainerWidget>
#include <iostream>
#include <Wt/WString>

// using namespace Wt;
// using namespace Wt::Ext;

/* WizardApplication.h */
class WizardApplication : public Wt::WApplication {
public:
    WizardApplication( const Wt::WEnvironment& env);

private slots:
    void registrationWizard();

};

/* WizardApplication.C */

WizardApplication::WizardApplication(const Wt::WEnvironment& env) : Wt::WApplication(env) {

  setTitle("Wizard Demo");
  useStyleSheet("wizard.css");

  root()->addWidget( new Wt::WText("Click on the button to start the wizard") );

  Wt::Ext::Button *b = new Wt::Ext::Button("Start registration", root());
  b->setMargin(5, Wt::Left);

  b->clicked().connect( SLOT(this, WizardApplication::registrationWizard) );

}

void WizardApplication::registrationWizard() {

    Wt::Ext::Wizard wizard;
    wizard.setWindowTitle("Register your product" );

    Wt::Ext::WizardPage* w = new Wt::Ext::WizardPage();
    w->setTitle("Introduction");
    new Wt::WText("This wizard will help you register your copy of Super Product One(TM)", w);
    wizard.addPage( w );

    Wt::Ext::WizardPage* w2 = new Wt::Ext::WizardPage();
    w2->setTitle("Fill in your data");
    Wt::WLabel* nameLabel = new Wt::WLabel("Name: ", w2);
    Wt::Ext::LineEdit* userNameEdit = new Wt::Ext::LineEdit("Enter your name", w2);
    nameLabel->setBuddy(userNameEdit);
    w2->registerField( FIELD( user*, userNameEdit, Wt::Ext::LineEdit::text) );
    wizard.addPage( w2 );

    Wt::Ext::WizardPage* w3 = new Wt::Ext::WizardPage();
    w3->setTitle("Conclusion");
    new Wt::WText("Thank you!", w3);
    new Wt::WText("This copy of Super Product One(TM) has been successfully been registered to " + boost::any_cast<const Wt::WString&>(wizard.field("user")), w3 );
#ifdef _DEBUG
    std::cout << "Product successfully registered to " << userNameEdit->text() << std::endl;
#endif
    wizard.addPage( w3 );

    wizard.show();
#ifdef _DEBUG
    std::cout << "Product successfully registered to " << boost::any_cast<const Wt::WString&>(wizard.field("user")) << std::endl;
#endif

}

Wt::WApplication *createApplication(const Wt::WEnvironment& env) {
    WizardApplication* wapp = new WizardApplication(env);

    return wapp;
}

int main(int argc, char **argv)
{
   return Wt::WRun(argc, argv, &createApplication);
}
